# Capitulo 2
# Ejercicio 4
# Autor = "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec

# Asume que ejecutamos las siguietes sentencias de asignacón

Ancho = 17
Alto = 12.0
A = float (Ancho/2)
B = float (Ancho/2.0)
C = int (Alto/3)
D = int (1+2*5)

print("",Ancho,"/2 = ", A )
print("",Ancho,"/2.0 = ", B )
print("",Ancho,"/3 = ", C )
print(" 1+2*5", D )