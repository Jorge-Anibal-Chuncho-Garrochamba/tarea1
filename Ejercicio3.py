# Capitulo 2
# Ejercicio 3
# Autor = "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec"

# Escribe un programa para pedirle al usuario el número de horas y la tarifa por hora para calcular el salario bruto

Horas = float(input("Introduzca el número de horas"))
Tarifa = (Horas * float(input("Introduzca la tarifa por hora")))
print("Su salario es", Tarifa)