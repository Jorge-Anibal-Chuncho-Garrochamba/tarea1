# Capitulo 2
# Ejercicio 5
# Autor = "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec

# Escribe un programa que le pida al usuario una tamperatura en grados celsius, la convierta a grados Fahrenheit e imprima por pantalla la temperatura convertida

Centigrados = float(input("Ingrese la cantidad de Grados Centigrados: "))
Fahrenheit = (Centigrados * 9/5) + 32
print("La temperatura en grados Fahrenheit es: ", Fahrenheit)